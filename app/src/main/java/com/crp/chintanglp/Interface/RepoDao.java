package com.crp.chintanglp.Interface;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.crp.chintanglp.DataPojo.UserDatum;

import java.util.List;

@Dao
public interface RepoDao {

    @Query("SELECT * FROM UserDatum")
    List<UserDatum> getAllRepos();

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(UserDatum... repos);

    @Update
    void update(UserDatum... repos);

    @Delete
    void delete(UserDatum... repos);

    @Query("DELETE FROM UserDatum")
    void deleteAll();
}