package com.crp.chintanglp.DataBase;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.crp.chintanglp.DataPojo.UserDatum;
import com.crp.chintanglp.Interface.RepoDao;

@Database(entities = {UserDatum.class}, version = 1, exportSchema = false)
public abstract class RepoDatabase extends RoomDatabase {

    private static final String DB_NAME = "userDatabase.db";
    private static volatile RepoDatabase instance;

    public static synchronized RepoDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static RepoDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                RepoDatabase.class,
                DB_NAME).build();
    }

    public abstract RepoDao getRepoDao();
}