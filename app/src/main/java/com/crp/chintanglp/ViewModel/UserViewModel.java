package com.crp.chintanglp.ViewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.crp.chintanglp.Interface.Api;
import com.crp.chintanglp.DataPojo.UserDatum;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserViewModel extends ViewModel {


    //this is the data that we will fetch asynchronously
    private MutableLiveData<List<UserDatum>> userDataList;


    //we will call this method to get the data
    public LiveData<List<UserDatum>> getUserDatas() {
        //if the list is null
        if (userDataList == null) {
            userDataList = new MutableLiveData<List<UserDatum>>();
            //we will load it asynchronously from server in this method
            loadHeroes();
        }

        //finally we will return the list
        return userDataList;
    }
    //This method is using Retrofit to get the JSON data from URL
    private void loadHeroes() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<UserDatum>> call = api.getUserDatas();


        call.enqueue(new Callback<List<UserDatum>>() {
            @Override
            public void onResponse(Call<List<UserDatum>> call, Response<List<UserDatum>> response) {

                //finally we are setting the list to our MutableLiveData
                userDataList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<UserDatum>> call, Throwable t) {

            }
        });
    }

}
