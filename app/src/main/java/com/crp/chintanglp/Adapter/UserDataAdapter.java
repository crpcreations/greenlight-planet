package com.crp.chintanglp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crp.chintanglp.DataPojo.UserDatum;
import com.crp.chintanglp.R;

import java.util.List;

public class UserDataAdapter extends RecyclerView.Adapter<UserDataAdapter.UserDataView>{

    List<UserDatum> userDatas;
    Context context
            ;

    public UserDataAdapter(List<UserDatum> userDatas, Context context) {
        this.userDatas = userDatas;
        this.context = context;
    }

    @NonNull
    @Override
    public UserDataView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, viewGroup, false);
        return new UserDataView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserDataView userDataView, int i) {

        UserDatum datum=userDatas.get(i);

        ((TextView)userDataView.itemView.findViewById(R.id.name)).setText(datum.getName());
        ((TextView)userDataView.itemView.findViewById(R.id.company_name)).setText(datum.getWebsite());
        ((TextView)userDataView.itemView.findViewById(R.id.email_id)).setText(datum.getEmail());
        ((TextView)userDataView.itemView.findViewById(R.id.phone_no)).setText(datum.getPhone());
    }

    @Override
    public int getItemCount() {
        return userDatas.size();
    }

    class UserDataView extends RecyclerView.ViewHolder {
    public UserDataView(@NonNull View itemView) {
        super(itemView);
    }
}}
