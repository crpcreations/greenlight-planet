package com.crp.chintanglp;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.crp.chintanglp.Adapter.UserDataAdapter;
import com.crp.chintanglp.DataBase.RepoDatabase;
import com.crp.chintanglp.DataPojo.UserDatum;
import com.crp.chintanglp.Utility.CheckConnection;
import com.crp.chintanglp.ViewModel.UserViewModel;

import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);


        if (!CheckConnection.isConnectingToInternet(this)) {
            DeleteDB deleteDB = new DeleteDB();
            deleteDB.execute();
        }


        UserViewModel model = ViewModelProviders.of(this).get(UserViewModel.class);


        model.getUserDatas().observe(this, new Observer<List<UserDatum>>() {
            @Override
            public void onChanged(@Nullable List<UserDatum> userData) {

                progressBar.setVisibility(View.GONE);
                UserDataAdapter userDataAdapter = new UserDataAdapter(userData, HomeActivity.this);
                recyclerView.setAdapter(userDataAdapter);

                UpdateDB updateDB = new UpdateDB(userData);
                updateDB.execute();

            }
        });

        GetData getDat = new GetData();
        try {
            List<UserDatum> data = getDat.execute().get();
            Log.e("Size", String.valueOf(data.size()));
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


    class DeleteDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            RepoDatabase.getInstance(HomeActivity.this).getRepoDao().deleteAll();
            return null;
        }
    }


    class UpdateDB extends AsyncTask<Void, Void, Void> {

        List<UserDatum> datumList;

        UpdateDB(List<UserDatum> datumList) {
            this.datumList = datumList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (UserDatum datum : datumList) {
                RepoDatabase
                        .getInstance(HomeActivity.this)
                        .getRepoDao()
                        .insert(datum);
            }

            return null;
        }
    }


    class GetData extends AsyncTask<Void, Void, List<UserDatum>> {
        @Override
        protected List<UserDatum> doInBackground(Void... voids) {

            List<UserDatum> allRepos = RepoDatabase
                    .getInstance(HomeActivity.this)
                    .getRepoDao()
                    .getAllRepos();
            return allRepos;
        }
    }
}
